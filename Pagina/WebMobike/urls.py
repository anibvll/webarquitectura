from django.urls import path
from .views import index, mapa

urlpatterns = [
    path('', index, name = "index"),
    path('mapa/', mapa, name = "mapa"),
]
